jquery-externallink (v2.0.1)
===================

This 'shim' lets [rel='external'] behave like the deprecated [target='_blank']


## Installation

Just use `bower install --save jquery-externallink`

## Usage

```js
//*** Minimal use
$('a').externalLink();

//*** Including options
$('a').externalLink({
    'setLabel': true, // Add or append (if title/alt already set) a label
    'label': 'Opens in a new window' // The actual label to add/append
});

```
Using the above examples, on domload jQuery externallink will check all currently available anchors.
When you want to apply juery-externallink on dynamically generated anchors, simply do `$('a').externalLink()` after generating the anchors.

### Options

```js
$('a').externalLink({
    'setLabel': true, // Add or append (if title/alt already set) a label
    'label': 'Opens in a new window' // The actual label to add/append
});
```

### CHANGELOG

- 2.0   Added optional options object
        **BREAKING CHANGE**: You'll have to manually call .externalLink() in your projects now.

- 1.1   Introduced event namespace '.externallink'. Using this namespace, one can easily 
 		unset all externallink-related events on an object using $('a').off('.externallink');

- 1.0 	First release

### Copyright & Authors

This package is brought to you by [Neverwoods Internet Technology](http://www.neverwoods.com/)

- [Felix Langfeldt](http://github.com/flangfeldt)
- [Robin van Baalen](http://github.com/rvanbaalen)

